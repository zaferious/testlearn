import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';


const routes: Routes = [
  {
    path: 'loginPage', component: LoginPageComponent
  },
  {
    path: '', redirectTo: '/loginPage', pathMatch: 'full'
  },
  {
    path: '**', redirectTo: '/loginPage'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
